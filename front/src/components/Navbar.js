import React from 'react'
import { Link, useNavigate } from 'react-router-dom';
import swal from 'sweetalert2';
import $ from 'jquery';

const Navbar = ({ module }) => {

    const navigate = useNavigate();
    const genre = localStorage.getItem("genre");
    const role = localStorage.getItem("role");
    const username = localStorage.getItem("username");

    const onClickMenu = () => {
        $('.page-container').toggleClass('sbar_collapsed')
    }

    const logOut = () => {
        swal.fire({
            icon: 'question',
            title: '¿Estas seguro?',
            text: '¿Desea cerrar sesión?',
            showCancelButton: true,
            confirmButtonText: 'Si',
            cancelButtonText: 'No'
        }).then((result) => {
            if (result.isConfirmed) {
                localStorage.setItem("role", "Invitado");
                navigate('/');
            }
        })
    }

    return (
        <div className="page-title-area">
            <div className="row py-2 align-items-center">
                <div className="col-sm-8">
                    <div className="nav-btn pull-left" onClick={onClickMenu}>
                        <span />
                        <span />
                        <span />
                    </div>
                    <div className="horizontal-menu pull-left">
                        <ul className="nav nav-pills">
                            <li className="nav-item">
                                <Link className={module === 'home' ? "nav-link active" : "nav-link"} to={role === "Invitado" ? "/" : (role === "Administrador" ? "/adminHome" : "/userHome")}><span><h6>Inicio</h6></span></Link>
                            </li>
                            {role === "Administrador" ?
                                <li className="nav-item">
                                    <Link className={module === 'user' ? "nav-link active" : "nav-link"} to={"/user"}><span><h6>Usuarios</h6></span></Link>
                                </li> : null}
                            <li className="nav-item">
                                <Link className={module === 'match' ? "nav-link active" : "nav-link"} to={"/match"}><span><h6>Eventos</h6></span></Link>
                            </li>
                            <li className="nav-item">
                                <Link className={module === 'team' ? "nav-link active" : "nav-link"} to={"/team"}><span><h6>Equipos</h6></span></Link>
                            </li>
                            <li className="nav-item">
                                <Link className={module === 'player' ? "nav-link active" : "nav-link"} to={"/player"}><span><h6>Jugadores</h6></span></Link>
                            </li>
                            <li className="nav-item">
                                <Link className={module === 'news' ? "nav-link active" : "nav-link"} to={"/news"}><span><h6>Noticias</h6></span></Link>
                            </li>
                        </ul>
                    </div>
                </div>
                <div className="col-sm-4 clearfix">
                    {role === "Invitado" ?
                        <div className="pull-right">
                            <Link role="button" to={"/login"} className="btn btn-outline-primary m-2 text-reset">
                                <h6>Iniciar sesión</h6>
                            </Link>
                            <Link role="button" to={"/register"} className="btn btn-primary m-2">
                                <h6>Registro</h6>
                            </Link>
                        </div> :
                        <div className="pull-right">
                            <Link className="rounded-circle" to={"#"} role="button" id="dropdownUser" data-toggle="dropdown">
                                <div className="avatar avatar-md avatar-indicators avatar-online">
                                    <img alt="avatar" src={genre === "Masculino"?(role==="Administrador"?"/assets/images/avatar/avatar-1.jpg":"/assets/images/avatar/avatar-3.jpg"):role==="Administrador"?"/assets/images/avatar/avatar-4.jpg":"/assets/images/avatar/avatar-2.jpg"} className="rounded-circle" />
                                </div>
                            </Link>
                            <div className="dropdown-menu">
                                <div className="px-4 pb-0 pt-2">
                                    <div>
                                        <h6 className="mb-1">{username}</h6>
                                        <p className="text-inherit">{role}</p>
                                    </div>
                                    <div className=" dropdown-divider mt-2 mb-2" />
                                </div>
                                <Link role='button' className="dropdown-item text-danger" onClick={logOut}><i className='fa fa-sign-out mr-2'/>Cerrar sesión</Link>
                            </div>
                        </div>}
                </div>
            </div>
        </div>
    )
}

export default Navbar;