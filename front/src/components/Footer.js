import React from 'react';
import { Link } from 'react-router-dom';

const Footer = () => {
    return (
        <footer>
            <div className="footer-area">
                <p>© Copyright 2022. All right reserved. Website by <Link to={"/"}>SportNews</Link>.</p>
            </div>
        </footer>
    )
}

export default Footer;