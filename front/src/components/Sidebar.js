import React from 'react'
import { Link } from 'react-router-dom';

const Sidebar = ({module}) => {
    const role = localStorage.getItem("role");
    return (
        <div className="sidebar-menu">
            <div className="sidebar-header">
                <div className="d-flex justify-content-center btn btn-link">
                    <Link to={role==="Invitado"?"/":(role==="Administrador"?"/adminHome":"/userHome")}><img src='/assets/images/icon/Logo.png' alt='logo'/></Link>
                </div>
            </div>
            <div className="main-menu">
                <div>
                    <nav>
                        <ul className="metismenu" id="menu">
                            <li className={module==='home'?"active":""}>
                                <Link to={role==="Invitado"?"/":(role==="Administrador"?"/adminHome":"/userHome")}><i className="fa fa-home"/><span>Inicio</span></Link>
                            </li>
                            {role === "Administrador"?
                            <li className={module==='user'?"active":""}>
                                <Link to={"/user"}><i className="fa fa-users"/><span>Usuarios</span></Link>
                            </li>:null}
                            <li className={module==='match'?"active":""}>
                                <Link to={"/match"}><i className="fa fa-soccer-ball-o"/><span>Eventos</span></Link>
                            </li>
                            <li className={module==='team'?"active":""}>
                                <Link to={"/team"}><i className="fa fa-flag-checkered"/><span>Equipos</span></Link>
                            </li>
                            <li className={module==='player'?"active":""}>
                                <Link to={"/player"}><i className="fa fa-child"/><span>Jugadores</span></Link>
                            </li>
                            <li className={module==='news'?"active":""}>
                                <Link to={"/news"}><i className="fa fa-newspaper-o"/><span>Noticias</span></Link>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    )
}

export default Sidebar;