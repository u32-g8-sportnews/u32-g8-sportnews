import { useNavigate } from "react-router-dom";
import APIInvoke from "./APIInvoke";

const navigate = useNavigate();

const validateSession = async () => {

    const response = await APIInvoke.invokePOST(`/auth/verify`, {});

    if (response.error !== undefined) {
        navigate('/login');
        return;
    }else{
        response.message.role;
    }
}

export default validateSession;