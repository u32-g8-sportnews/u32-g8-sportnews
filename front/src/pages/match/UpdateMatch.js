import React, { useState, useEffect } from 'react';
import Sidebar from '../../components/Sidebar';
import Footer from '../../components/Footer';
import Navbar from '../../components/Navbar';
import { Link, useNavigate, useParams } from 'react-router-dom';
import APIInvoke from '../../utils/APIInvoke';
import swal from 'sweetalert2';

const UpdateMatch = () => {

    let navigate = useNavigate();

    const { id } = useParams();

    useEffect(() => {
        async function loadMatch() {
            const response = await APIInvoke.invokeGET(`/match/${id}`);
            setMatch(response);
            return;
        }
        loadMatch();
        loadTeams();
        document.getElementById("seasonTournament").focus();
    }, []);

    const [match, setMatch] = useState(
        {
            nameTournament: '',
            seasonTournament: '',
            date: Date.toString,
            place: '',
            local: '',
            localScore: '',
            visitor: '',
            visitorScore: ''
        }
    )

    const { nameTournament, seasonTournament, date, place, local, localScore, visitor, visitorScore } = match;

    const [teams, setTeams] = useState([]);

    const loadTeams = async () => {
        const response = await APIInvoke.invokeGET(`/team/list`);
        setTeams(response);
    }

    const onChange = (e) => {
        setMatch({
            ...match,
            [e.target.name]: e.target.value
        });
    }

    const onSubmit = (e) => {
        e.preventDefault();
        updateMatch();
    }

    const updateMatch = async () => {

        const data = {
            nameTournament: match.nameTournament,
            seasonTournament: match.seasonTournament,
            date: match.date,
            place: match.place,
            local: match.local,
            localScore: match.localScore,
            visitor: match.visitor,
            visitorScore: match.visitorScore
        }

        const response = await APIInvoke.invokePUT(`/match/${id}`, data);

        if (response.message === "Match updated") {
            swal.fire({
                title: 'Enhorabuena!',
                icon: 'success',
                text: 'Evento actualizado satisfactoriamente',
                confirmButtonText: 'Ok',
                allowOutsideClick: false,
                allowEscapeKey: false,
            }).then((result) => {
                if (result.isConfirmed) {
                    navigate('/match');
                }
            })
        } else if (response.message === 'Match is already registered') {
            swal.fire({
                title: 'Lo sentimos!',
                icon: 'warning',
                text: 'El evento ya esta registrado',
                confirmButtonText: 'Ok',
                allowOutsideClick: false,
                allowEscapeKey: false,
            });
        } else {
            swal.fire({
                title: 'Upss!!',
                icon: 'error',
                text: 'Error desconocido',
                confirmButtonText: 'Ok',
                allowOutsideClick: false,
                allowEscapeKey: false,
            });
            console.log("Error: ", response);
        }
    }

    return (
        <div className="page-container">
            <Sidebar module={'match'} />
            <div className="main-content">
                <Navbar module={'match'} />
                <div className="main-content-inner">
                    <div className='row justify-content-center'>
                        <div className='col-lg-7'>
                            <div className="card mt-5 p-3">
                                <div className="card-body">
                                    <div className=" mb-5">
                                        <h4>Actualizar evento</h4>
                                    </div>
                                    <div>
                                        <form onSubmit={onSubmit}>
                                            {/* row */}
                                            <div className="mb-3 row">
                                                <label htmlFor="seasonTournament" className="col-sm-4 col-form-label form-label"><h6>Temporada</h6></label>
                                                <div className="col-md-8 col-12">
                                                    <input type="text" className="form-control form-control2" placeholder="Temporada actual del torneo" id="seasonTournament" name="seasonTournament" value={seasonTournament} onChange={onChange} required />
                                                </div>
                                            </div>
                                            {/* row */}
                                            <div className="mb-3 row">
                                                <label htmlFor="nameTournament" className="col-sm-4 col-form-label form-label"><h6>Torneo</h6></label>
                                                <div className="col-md-8 col-12">
                                                    <select id="nameTournament" name="nameTournament" className="form-select form-control2" value={nameTournament} onChange={onChange} required>
                                                        <option value={""} defaultValue disabled>Seleccione el nombre del torneo</option>
                                                        <option value={"Liga BetPlay"}>Liga BetPlay</option>
                                                        <option value={"Liga Regional"}>Liga Regional</option>
                                                        <option value={"Superliga"}>Superliga</option>
                                                        <option value={"Torneo BetPlay"}>Torneo BetPlay</option>
                                                    </select>
                                                </div>
                                            </div>
                                            {/* row */}
                                            <div className="mb-3 row">
                                                <label htmlFor="date" className="col-sm-4 col-form-label form-label"><h6>Fecha del evento</h6></label>
                                                <div className="col-md-8 col-12">
                                                    <input type="datetime-local" className="form-control form-control2" placeholder="Fecha del evento" id="date" name="date" value={date} onChange={onChange} required/>
                                                </div>
                                            </div>
                                            {/* row */}
                                            <div className="mb-3 row">
                                                <label htmlFor="place" className="col-sm-4 col-form-label form-label"><h6>Lugar</h6></label>
                                                <div className="col-md-8 col-12">
                                                    <input type="text" className="form-control form-control2" placeholder="Lugar del evento" id="place" name="place" value={place} onChange={onChange} required/>
                                                </div>
                                            </div>
                                            {/* row */}
                                            <div className="mb-3 row">
                                                <label htmlFor="local" className="col-sm-4 col-form-label form-label"><h6>Equipo Local</h6></label>
                                                <div className="col-md-8 col-12">
                                                    <select id="local" name="local" className="form-select form-control2" value={local} onChange={onChange} required >
                                                        <option value={""} defaultValue disabled>Seleccione el equipo local</option>
                                                        {teams.map(
                                                            item =>
                                                        <option key={item._id} value={item.name}>{item.name}</option>)}
                                                    </select>
                                                </div>
                                            </div>
                                            {/* row */}
                                            <div className="mb-3 row">
                                                <label htmlFor="localScore" className="col-sm-4 col-form-label form-label"><h6>Marcador Local</h6></label>
                                                <div className="col-md-8 col-12">
                                                    <input type="text" className="form-control form-control2" placeholder="Marcador del equipo local" id="localScore" name="localScore" value={localScore} onChange={onChange}/>
                                                </div>
                                            </div>
                                            {/* row */}
                                            <div className="mb-3 row">
                                                <label htmlFor="visitor" className="col-sm-4 col-form-label form-label"><h6>Equipo Visitante</h6></label>
                                                <div className="col-md-8 col-12">
                                                    <select id="visitor" name="visitor" className="form-select form-control2" value={visitor} onChange={onChange} required >
                                                        <option value={""} defaultValue disabled>Seleccione el equipo visitante</option>
                                                        {teams.map(
                                                            item =>
                                                        <option key={item._id} value={item.name}>{item.name}</option>)}
                                                    </select>
                                                </div>
                                            </div>
                                            {/* row */}
                                            <div className="mb-3 row">
                                                <label htmlFor="visitorScore" className="col-sm-4 col-form-label form-label"><h6>Marcador Visitante</h6></label>
                                                <div className="col-md-8 col-12">
                                                    <input type="text" className="form-control form-control2" placeholder="Marcador del equipo visitante" id="visitorScore" name="visitorScore" value={visitorScore} onChange={onChange}/>
                                                </div>
                                            </div>
                                            <div className="row d-flex justify-content-center pt-4">
                                                <button type="submit" className="btn btn-primary mr-3"><i className="fa fa-save mr-2"></i>Guardar</button>
                                                <Link role="button" to={"/match"} className="btn btn-danger"><i className="fa fa-close mr-2"></i>Cancelar</Link>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <Footer />
        </div>
    )
}

export default UpdateMatch;