import React, { useState, useEffect } from 'react';
import Sidebar from '../../components/Sidebar';
import Footer from '../../components/Footer';
import Navbar from '../../components/Navbar';
import { Link } from 'react-router-dom';
import APIInvoke from '../../utils/APIInvoke';
import swal from 'sweetalert2';

const Match = () => {
    const role = localStorage.getItem("role");

    const [matches, setMatches] = useState([]);
    const [teams, setTeams] = useState([]);
    const [search, setSearch] = useState("");
    

    useEffect(() => {
        loadMatches();
        loadTeams();
    }, [])

    const searchEvent = async () => {
        const response = await APIInvoke.invokeGET(`/match/list/${search}`);
        setMatches(response);
    };

    const onSubmit = (e) => {
        e.preventDefault();
        searchEvent();
    }

    const onChange = (e) => {
        setSearch(e.target.value);
    }

    const loadTeams = async () => {
        const response = await APIInvoke.invokeGET(`/team/list`);
        setTeams(response);
    }

    const loadMatches = async () => {
        const response = await APIInvoke.invokeGET(`/match/list`);
        setMatches(response);
    }

    const deleteMatches = async (e, id) => {

        e.preventDefault();

        swal.fire({
            icon: 'question',
            title: '¿Estas seguro?',
            text: '¿Deseas eliminar este evento?',
            showCancelButton: true,
            confirmButtonText: 'Eliminar',
            cancelButtonText: 'Cancelar'
        }).then(async (result) => {
            if (result.isConfirmed) {
                const response = await APIInvoke.invokeDELETE(`/match/${id}`);
                if (response.message === "Match deleted") {
                    swal.fire(
                        'Eliminación exitosa',
                        'El evento fue eliminado',
                        'success'
                    )
                    loadMatches();
                }
            }
        })
    }
    return (
        <div className="page-container">
            <Sidebar module={'match'} />
            <div className="main-content">
                <Navbar module={'match'} />
                <div className="main-content-inner mt-4">
                    <div className="card mt-5 p-3">
                        <div className='card-body'>
                            <div className="trd-history-tabs d-flex align-items-left flex-column">
                                <h4 className="header-title">Eventos</h4>
                                <div>
                                    <div className="search-box pull-left col-lg-6 p-0">
                                        <form onSubmit={onSubmit}>
                                            <input className='w-100 form-control input-rounded' type="text" name="search" placeholder="Buscar eventos por equipos, torneo, o fecha" value={search} onChange={onChange} />
                                            <i className="ti-search" />
                                        </form>
                                    </div>
                                    {role === "Administrador" ?
                                        <div className="d-flex pull-right align-middle">
                                            <Link to={"/match/create"} className="btn btn-primary"><i className="fa fa-plus-circle mr-2" />Agregar evento</Link>
                                        </div> : null}
                                </div>
                                <div className="row">
                                    {
                                        matches.map(
                                            item =>
                                                <div key={item._id} className="col-lg-6 mt-5">
                                                    <div className="card card-bordered">
                                                        <div className="card-body pb-3">
                                                            <div className="d-sm-flex justify-content-center align-items-center">
                                                                <div className="trd-history-tabs">
                                                                    <ul className="nav">
                                                                        <li>
                                                                            <Link className="active" to={"#"}>{item.nameTournament}</Link>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                            <div className="exhcange-rate d-sm-flex justify-content-center align-items-center mt-4">
                                                                <div className='d-flex justify-content-between align-items-center w-75'>
                                                                    <div className='d-flex align-items-center flex-column'>
                                                                        <div>
                                                                            <img src={teams.filter(team => team.name===item.local).map(i=>i.image)} className="rounded-square team-md" alt="team" />
                                                                        </div>
                                                                        <p className='mt-1 text-center'>{item.local}</p>
                                                                    </div>
                                                                    <h6>VS</h6>
                                                                    <div className='d-flex align-items-center flex-column'>
                                                                        <div>
                                                                            <img src={teams.filter(team => team.name===item.visitor).map(i=>i.image)} className="rounded-square team-md" alt="team" />
                                                                        </div>
                                                                        <p className='mt-1 text-center'>{item.visitor}</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="exhcange-rate d-flex align-items-center flex-column mt-3 p-3">
                                                                <p>{item.date.split("T")[0]} | {item.date.split("T")[1]}</p>
                                                                <p>{item.place}</p>
                                                            </div>
                                                        </div>
                                                        {role === "Administrador" ?
                                                            <div className="d-flex justify-content-center pb-3">
                                                                <Link to={`/match/update/${item._id}`} className="btn btn-primary btn-xs mr-2"><i className="fa fa-edit" /></Link>
                                                                <Link to={"#"} role='button' onClick={(e) => { deleteMatches(e, item._id) }} className="btn btn-danger btn-xs"><i className="fa fa-trash-o" /></Link>
                                                            </div> : null}
                                                    </div>
                                                </div>
                                        )
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <Footer />
        </div>
    )
}

export default Match