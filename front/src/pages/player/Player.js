import React, { useState, useEffect } from 'react';
import Sidebar from '../../components/Sidebar';
import Footer from '../../components/Footer';
import Navbar from '../../components/Navbar';
import { Link } from 'react-router-dom';
import APIInvoke from '../../utils/APIInvoke';
import swal from 'sweetalert2';

const Player = () => {

    const role = localStorage.getItem("role");

    const [players, setPlayers] = useState([]);
    const [search, setSearch] = useState("");

    useEffect(() => {
        loadPlayers();
    }, [])

    const searchEvent = async () => {
        const response = await APIInvoke.invokeGET(`/player/list/${search}`);
        setPlayers(response);
    };

    const onSubmit = (e) => {
        e.preventDefault();
        searchEvent();
    }

    const onChange = (e) => {
        setSearch(e.target.value);
    }

    const loadPlayers = async () => {
        const response = await APIInvoke.invokeGET(`/player/list`);
        setPlayers(response);
    }

    const deletePlayer = async (e, id) => {

        e.preventDefault();

        swal.fire({
            icon: 'question',
            title: '¿Estas seguro?',
            text: '¿Deseas eliminar este jugador?',
            showCancelButton: true,
            confirmButtonText: 'Eliminar',
            cancelButtonText: 'Cancelar'
        }).then(async (result) => {
            if (result.isConfirmed) {
                const response = await APIInvoke.invokeDELETE(`/player/${id}`);
                if (response.message === "Player deleted") {
                    swal.fire(
                        'Eliminación exitosa',
                        'El jugador fue eliminado',
                        'success'
                    )
                    loadPlayers();
                }
            }
        })
    }

    return (
        <div className="page-container">
            <Sidebar module={'player'} />
            <div className="main-content">
                <Navbar module={'player'} />
                <div className="main-content-inner">
                    <div className="card mt-5 p-3">
                        <div className='card-body'>
                            <div className="trd-history-tabs d-flex align-items-left flex-column">
                                <h4 className="header-title">Jugadores</h4>
                                <div>
                                    <div className="search-box pull-left col-lg-6 p-0">
                                        <form onSubmit={onSubmit}>
                                            <input className='w-100 form-control input-rounded' type="text" name="search" placeholder="Buscar jugadores por nombre, equipo, o posición" value={search} onChange={onChange} />
                                            <i className="ti-search" />
                                        </form>
                                    </div>
                                    {role === "Administrador" ?
                                        <div className="d-flex pull-right align-middle">
                                            <Link to={"/player/create"} className="btn btn-primary"><i className="fa fa-plus-circle mr-2" />Agregar jugador</Link>
                                        </div> : null}
                                </div>
                                <div className="row">
                                    {
                                        players.map(
                                            item =>
                                                <div key={item._id} className="col-lg-3 col-md-6 mt-5">
                                                    <div className="card card-bordered">
                                                        <img className="card-img-top img-fluid" src={item.image} alt="players" />
                                                        <div className="card-body d-flex align-items-center flex-column">
                                                            <h5 className="title">{item.name}</h5>
                                                            <p className="card-text">{item.position}</p>
                                                            {role === "Administrador" ?
                                                                <div className="d-flex justify-content-center">
                                                                    <Link to={`/player/update/${item._id}`} className="btn btn-primary btn-xs mr-2"><i className="fa fa-edit"/></Link>
                                                                    <Link to={"#"} role='button' onClick={(e) => { deletePlayer(e, item._id) }} className="btn btn-danger btn-xs"><i className="fa fa-trash-o" /></Link>
                                                                </div> : null}
                                                        </div>
                                                    </div>
                                                </div>
                                        )
                                    }
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <Footer />
        </div>
    )
}

export default Player