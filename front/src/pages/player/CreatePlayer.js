import React, { useState, useEffect } from 'react';
import Sidebar from '../../components/Sidebar';
import Footer from '../../components/Footer';
import Navbar from '../../components/Navbar';
import { Link, useNavigate } from 'react-router-dom';
import APIInvoke from '../../utils/APIInvoke';
import swal from 'sweetalert2';

const CreatePlayer = () => {

    let navigate = useNavigate();

    useEffect(() => {
        document.getElementById("name").focus();
        loadTeams();
    }, []);

    const [player, setPlayer] = useState(
        {
            name: '',
            age: '',
            image: '',
            position: '',
            nacionality: '',
            nameTeam: ''
        }
    )

    const { name, age, image, position, nacionality, nameTeam } = player;

    const [teams, setTeams] = useState([]);

    const loadTeams = async () => {
        const response = await APIInvoke.invokeGET(`/team/list`);
        setTeams(response);
    }

    const onChange = (e) => {
        setPlayer({
            ...player,
            [e.target.name]: e.target.value
        });
    }

    const onSubmit = (e) => {
        e.preventDefault();
        savePlayer();
    }

    const savePlayer = async () => {

        const data = {
            name: player.name,
            age: player.age,
            image: player.image,
            position: player.position,
            nacionality: player.nacionality,
            nameTeam: player.nameTeam
        }

        const response = await APIInvoke.invokePOST(`/player/save`, data);

        if (response.message === "Player saved") {
            swal.fire({
                title: 'Enhorabuena!',
                icon: 'success',
                text: 'Jugador registrado satisfactoriamente',
                confirmButtonText: 'Ok',
                allowOutsideClick: false,
                allowEscapeKey: false,
            }).then((result) => {
                if (result.isConfirmed) {
                    navigate('/player');
                }
            })
        } else if (response.message === 'Player is already registered') {
            swal.fire({
                title: 'Lo sentimos!',
                icon: 'warning',
                text: 'El jugador ya esta registrado',
                confirmButtonText: 'Ok',
                allowOutsideClick: false,
                allowEscapeKey: false,
            });
        } else {
            swal.fire({
                title: 'Upss!!',
                icon: 'error',
                text: 'Error desconocido',
                confirmButtonText: 'Ok',
                allowOutsideClick: false,
                allowEscapeKey: false,
            });
            console.log("Error: ", response);
        }
    }

    return (
        <div className="page-container">
            <Sidebar module={'player'} />
            <div className="main-content">
                <Navbar module={'player'} />
                <div className="main-content-inner">
                    <div className='row justify-content-center'>
                        <div className='col-lg-7'>
                            <div className="card mt-5 p-3">
                                <div className="card-body">
                                    <div className=" mb-5">
                                        <h4>Agregar jugador</h4>
                                    </div>
                                    <div>
                                        <form onSubmit={onSubmit}>
                                            {/* row */}
                                            <div className="mb-3 row">
                                                <label htmlFor="name" className="col-sm-4 col-form-label form-label"><h6>Nombre</h6></label>
                                                <div className="col-md-8 col-12">
                                                    <input type="text" className="form-control form-control2" placeholder="Nombre del jugador" id="name" name='name' value={name} onChange={onChange} required />
                                                </div>
                                            </div>
                                            {/* row */}
                                            <div className="mb-3 row">
                                                <label htmlFor="age" className="col-sm-4 col-form-label form-label"><h6>Edad</h6></label>
                                                <div className="col-md-8 col-12">
                                                    <input type="text" className="form-control form-control2" placeholder="Edad del jugador" id="age" name="age" value={age} onChange={onChange} required />
                                                </div>
                                            </div>
                                            {/* row */}
                                            <div className="mb-3 row">
                                                <label htmlFor="image" className="col-sm-4 col-form-label form-label"><h6>Imagen</h6></label>
                                                <div className="col-md-8 col-12">
                                                    <input type="text" className="form-control form-control2" placeholder="Url imagen del equipo" id="image" name="image" value={image} onChange={onChange} />
                                                </div>
                                            </div>
                                            {/* row */}
                                            <div className="mb-3 row">
                                                <label htmlFor="position" className="col-sm-4 col-form-label form-label"><h6>Posición</h6></label>
                                                <div className="col-md-8 col-12">
                                                    <select id="position" name="position" className="form-select form-control2" value={position} onChange={onChange} required >
                                                        <option value={""} defaultValue disabled>Seleccione la posición del jugador</option>
                                                        <option value={"Arquero"}>Arquero</option>
                                                        <option value={"Defensa"}>Defensa</option>
                                                        <option value={"Delantero"}>Delantero</option>
                                                        <option value={"Extremo"}>Extremo</option>
                                                        <option value={"Lateral"}>Lateral</option>
                                                        <option value={"Volante"}>Volante</option>
                                                    </select>
                                                </div>
                                            </div>
                                            {/* row */}
                                            <div className="mb-3 row">
                                                <label htmlFor="nacionality" className="col-sm-4 col-form-label form-label"><h6>Nacionalidad</h6></label>
                                                <div className="col-md-8 col-12">
                                                    <select id="nacionality" name="nacionality" className="form-select form-control2" value={nacionality} onChange={onChange} required >
                                                        <option value={""} defaultValue disabled>Seleccione la nacionalidad del jugador</option>
                                                        <option value={"Colombiano"}>Colombiano</option>
                                                        <option value={"Brasileño"}>Brasileño</option>
                                                        <option value={"Venezolano"}>Venezolano</option>
                                                    </select>
                                                </div>
                                            </div>
                                            {/* row */}
                                            <div className="mb-3 row">
                                                <label htmlFor="nameTeam" className="col-sm-4 col-form-label form-label"><h6>Equipo</h6></label>
                                                <div className="col-md-8 col-12">
                                                    <select id="nameTeam" name="nameTeam" className="form-select form-control2" value={nameTeam} onChange={onChange} required >
                                                        <option value={""} defaultValue disabled>Seleccione el equipo</option>
                                                        {teams.map(
                                                            item =>
                                                                <option key={item._id} value={item.name}>{item.name}</option>)}
                                                    </select>
                                                </div>
                                            </div>
                                            <div className="row d-flex justify-content-center pt-4">
                                                <button type="submit" className="btn btn-primary mr-3"><i className="fa fa-save mr-2"></i>Guardar</button>
                                                <Link role="button" to={"/player"} className="btn btn-danger"><i className="fa fa-close mr-2"></i>Cancelar</Link>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <Footer />
        </div>
    )
}

export default CreatePlayer;