import React, { useState, useEffect } from 'react';
import Sidebar from '../../components/Sidebar';
import Footer from '../../components/Footer';
import Navbar from '../../components/Navbar';
import { Link, useNavigate } from 'react-router-dom';
import APIInvoke from '../../utils/APIInvoke';
import swal from 'sweetalert2';

const CreateNews = () => {

    let navigate = useNavigate();

    useEffect(() => {
        document.getElementById("title").focus();
    }, []);

    const [news, setNews] = useState(
        {
            title: '',
            content: '',
            image: '',
            keywords: ''
        }
    )

    const { title, content, image, keywords } = news;

    const onChange = (e) => {
        setNews({
            ...news,
            [e.target.name]: e.target.value
        });
    }

    const onSubmit = (e) => {
        e.preventDefault();
        saveNews();
    }

    const saveNews = async () => {

        const data = {
            title: news.title,
            content: news.content,
            image: news.image,
            keywords: news.keywords
        }

        const response = await APIInvoke.invokePOST(`/news/save`, data);

        if (response.message === "News saved") {
            swal.fire({
                title: 'Enhorabuena!',
                icon: 'success',
                text: 'Noticia publicada satisfactoriamente',
                confirmButtonText: 'Ok',
                allowOutsideClick: false,
                allowEscapeKey: false,
            }).then((result) => {
                if (result.isConfirmed) {
                    navigate('/news');
                }
            })
        } else {
            swal.fire({
                title: 'Upss!!',
                icon: 'error',
                text: 'Error desconocido',
                confirmButtonText: 'Ok',
                allowOutsideClick: false,
                allowEscapeKey: false,
            });
            console.log("Error: ", response);
        }

    }

    return (
        <div className="page-container">
            <Sidebar module={'news'} />
            <div className="main-content">
                <Navbar module={'news'} />
                <div className="main-content-inner">
                    <div className='row justify-content-center'>
                    <div className='col-lg-7'>
                    <div className="card mt-5 p-3">
                        <div className="card-body">
                            <div className=" mb-5">
                                <h4>Publicar noticia</h4>
                            </div>
                            <div>
                                <form onSubmit={ onSubmit }>
                                    {/* row */}
                                    <div className="mb-3 row">
                                        <label htmlFor="title" className="col-sm-4 col-form-label form-label"><h6>Título</h6></label>
                                        <div className="col-sm-8 mb-3 mb-lg-0">
                                            <input type="text" className="form-control form-control2" placeholder="Título de la noticia" id="title" name="title" value={title} onChange={onChange} required />
                                        </div>
                                    </div>
                                    {/* row */}
                                    <div className="mb-3 row">
                                        <label htmlFor="content" className="col-sm-4 col-form-label form-label"><h6>Contenido</h6></label>
                                        <div className="col-md-8 col-12">
                                            <textarea rows={6} className="form-control form-control2" placeholder="Contenido de la noticia" id="content" name='content' value={content} onChange={onChange} required />
                                        </div>
                                    </div>
                                    {/* row */}
                                    <div className="mb-3 row">
                                        <label htmlFor="image" className="col-sm-4 col-form-label form-label"><h6>Imagen</h6></label>
                                        <div className="col-md-8 col-12">
                                            <input type="text" className="form-control form-control2" placeholder="Url imagen de la noticia" id="image" name="image" value={image} onChange={onChange} required />
                                        </div>
                                    </div>
                                    {/* row */}
                                    <div className="mb-3 row">
                                        <label htmlFor="keywords" className="col-sm-4 col-form-label form-label"><h6>Palabras Clave</h6></label>
                                        <div className="col-md-8 col-12">
                                            <input type="text" className="form-control form-control2" placeholder="Palabras clave" id="keywords" name='keywords' value={keywords} onChange={onChange} required />
                                        </div>
                                    </div>
                                    <div className="row d-flex justify-content-center pt-4">
                                            <button type="submit" className="btn btn-primary mr-3"><i className="fa fa-save mr-2"></i>Guardar</button>
                                            <Link role="button" to={"/news"} className="btn btn-danger"><i className="fa fa-close mr-2"></i>Cancelar</Link>
                                        </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    </div>
                    </div>
                </div>
            </div>
            <Footer />
        </div>
    )
}

export default CreateNews;