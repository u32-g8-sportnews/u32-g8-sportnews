import React, { useState, useEffect } from 'react';
import Sidebar from '../../components/Sidebar';
import Footer from '../../components/Footer';
import Navbar from '../../components/Navbar';
import { Link } from 'react-router-dom';
import APIInvoke from '../../utils/APIInvoke';
import swal from 'sweetalert2';

const News = () => {
    const role = localStorage.getItem("role");

    const [news, setNews] = useState([]);
    const [search, setSearch] = useState("");

    useEffect(() => {
        loadNews();
    }, [])

    const searchEvent = async () => {
        const response = await APIInvoke.invokeGET(`/news/list/${search}`);
        setNews(response);
    };

    const onSubmit = (e) => {
        e.preventDefault();
        searchEvent();
    }
    
    const onChange = (e) => { 
        setSearch( e.target.value );    
    }

    const loadNews = async () => {
        const response = await APIInvoke.invokeGET(`/news/list`);
        setNews(response);
    }

    const deleteNews = async (e, id) => {

        e.preventDefault();

        swal.fire({
            icon: 'question',
            title: '¿Estas seguro?',
            text: '¿Deseas eliminar la noticia?',
            showCancelButton: true,
            confirmButtonText: 'Eliminar',
            cancelButtonText: 'Cancelar'
        }).then(async (result) => {
            if (result.isConfirmed) {
                const response = await APIInvoke.invokeDELETE(`/team/${id}`);
                if (response.message === "News deleted") {
                    swal.fire(
                        'Eliminación exitosa',
                        'La noticia fue eliminada',
                        'success'
                    )
                    loadNews();
                }
            }
        })
    }

    return (
        <div className="page-container">
            <Sidebar module={'news'} />
            <div className="main-content">
                <Navbar module={'news'} />
                <div className="main-content-inner">
                    <div className="card mt-5 p-3">
                        <div className='card-body'>
                            <div className="trd-history-tabs d-flex align-items-left flex-column">
                                <h4 className="header-title">Noticias</h4>
                                <div>
                                    <div className="search-box pull-left col-lg-6 p-0">
                                        <form onSubmit={onSubmit}>
                                            <input className='w-100 form-control input-rounded' type="text" name="search" placeholder="Buscar noticias" value={search} onChange={onChange}/>
                                            <i className="ti-search" />
                                        </form>
                                    </div>
                                    {role === "Administrador" ?
                                        <div className="d-flex pull-right align-middle">
                                            <Link to={"/news/create"} className="btn btn-primary"><i className="fa fa-plus-circle mr-2" />Agregar noticia</Link>
                                        </div> : null}
                                </div>
                                <div className="row">
                                    {
                                        news.map(
                                            item =>
                                                <div key={item._id} className="col-lg-4 col-md-6 mt-5">
                                                    <div className="card card-bordered">
                                                        <img className="card-img-top img-fluid" src={item.image?item.image:"/assets/images/news/news.jpg"} alt="news" />
                                                        <div className="card-body">
                                                            <h5 className="title">{item.title}</h5>
                                                            <p className="card-text">{item.content}</p>
                                                            {role === "Administrador" ?
                                                                <div className="d-flex justify-content-left">
                                                                    <Link to={`/news/update/${item._id}`} className="btn btn-primary btn-xs mx-3"><i className="fa fa-edit" /></Link>
                                                                    <Link to={"#"} role='button' onClick={(e) => { deleteNews(e, item._id) }} className="btn btn-danger btn-xs"><i className="fa fa-trash-o" /></Link>
                                                                </div> :null}
                                                        </div>
                                                    </div>
                                                </div>
                                        )
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <Footer />
        </div>
    )
}

export default News