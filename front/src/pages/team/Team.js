import React, { useState, useEffect } from 'react';
import Sidebar from '../../components/Sidebar';
import Footer from '../../components/Footer';
import Navbar from '../../components/Navbar';
import { Link } from 'react-router-dom';
import APIInvoke from '../../utils/APIInvoke';
import swal from 'sweetalert2';

const Team = () => {

    const role = localStorage.getItem("role");

    const [teams, setTeams] = useState([]);
    const [search, setSearch] = useState("");

    useEffect(() => {
        loadTeams();
    }, [])

    const searchEvent = async () => {
        const response = await APIInvoke.invokeGET(`/team/list/${search}`);
        setTeams(response);
    };

    const onSubmit = (e) => {
        e.preventDefault();
        searchEvent();
    }

    const onChange = (e) => {
        setSearch(e.target.value);
    }

    const loadTeams = async () => {
        const response = await APIInvoke.invokeGET(`/team/list`);
        setTeams(response);
    }

    const deleteTeam = async (e, id) => {

        e.preventDefault();

        swal.fire({
            icon: 'question',
            title: '¿Estas seguro?',
            text: '¿Deseas eliminar el equipo?',
            showCancelButton: true,
            confirmButtonText: 'Eliminar',
            cancelButtonText: 'Cancelar'
        }).then(async (result) => {
            if (result.isConfirmed) {
                const response = await APIInvoke.invokeDELETE(`/team/${id}`);
                if (response.message === "Team deleted") {
                    swal.fire(
                        'Eliminación exitosa',
                        'El equipo fue eliminado',
                        'success'
                    )
                    loadTeams();
                }
            }
        })
    }

    return (
        <div className="page-container">
            <Sidebar module={'team'} />
            <div className="main-content">
                <Navbar module={'team'} />
                <div className="main-content-inner">
                    <div className="card mt-5 p-3">
                        <div className='card-body'>
                            <div className="trd-history-tabs d-flex align-items-left flex-column">
                                <h4 className="header-title">Equipos</h4>
                                <div>
                                    <div className="search-box pull-left col-lg-6 p-0">
                                        <form onSubmit={onSubmit}>
                                            <input className='w-100 form-control input-rounded' type="text" name="search" placeholder="Buscar equipos por nombre, país o ciudad" value={search} onChange={onChange} />
                                            <i className="ti-search" />
                                        </form>
                                    </div>
                                    {role === "Administrador" ?
                                        <div className="d-flex pull-right align-middle">
                                            <Link to={"/team/create"} className="btn btn-primary"><i className="fa fa-plus-circle mr-2" />Agregar equipo</Link>
                                        </div> : null}
                                </div>
                                <div className="row">
                                    {
                                        teams.map(
                                            item =>
                                                <div key={item._id} className="col-lg-3 col-md-6 mt-5">
                                                    <div className="card card-bordered">
                                                        <img className="px-4 pt-4 img-fluid" src={item.image} alt="teams" />
                                                        <div className="px-4 pb-4  d-flex align-items-center flex-column">
                                                            <h6 className="title">{item.name}</h6>
                                                            {role === "Administrador" ?
                                                                <div className="d-flex justify-content-center">
                                                                    <Link to={`/team/update/${item._id}`} className="btn btn-primary btn-xs mr-2"><i className="fa fa-edit" /></Link>
                                                                    <Link to={"#"} role='button' onClick={(e) => { deleteTeam(e, item._id) }} className="btn btn-danger btn-xs"><i className="fa fa-trash-o" /></Link>
                                                                </div> : null}
                                                        </div>
                                                    </div>
                                                </div>
                                        )
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <Footer />
        </div>
    )
}

export default Team