import React, { useState, useEffect } from 'react';
import Sidebar from '../../components/Sidebar';
import Footer from '../../components/Footer';
import Navbar from '../../components/Navbar';
import { Link, useNavigate } from 'react-router-dom';
import APIInvoke from '../../utils/APIInvoke';
import swal from 'sweetalert2';

const CreateTeam = () => {

    let navigate = useNavigate();

    useEffect(() => {
        document.getElementById("name").focus();
    }, []);

    const [team, setTeam] = useState(
        {
            name: '',
            image: '',
            country: '',
            city: '',
            founded: Date.toString,
            coach: ''
        }
    )

    const { name, image, country, city, founded, coach } = team;

    const onChange = (e) => {
        setTeam({
            ...team,
            [e.target.name]: e.target.value
        });
    }

    const onSubmit = (e) => {
        e.preventDefault();
        saveTeam();
    }

    const saveTeam = async () => {

        const data = {
            name: team.name,
            image: team.image,
            country: team.country,
            city: team.city,
            founded: team.founded,
            coach: team.coach
        }

        const response = await APIInvoke.invokePOST(`/team/save`, data);

        if (response.message === "Team saved") {
            swal.fire({
                title: 'Enhorabuena!',
                icon: 'success',
                text: 'Equipo registrado satisfactoriamente',
                confirmButtonText: 'Ok',
                allowOutsideClick: false,
                allowEscapeKey: false,
            }).then((result) => {
                if (result.isConfirmed) {
                    navigate('/team');
                }
            })
        } else if (response.message === 'Team is already registered') {
            swal.fire({
                title: 'Lo sentimos!',
                icon: 'warning',
                text: 'El equipo ya esta registrado',
                confirmButtonText: 'Ok',
                allowOutsideClick: false,
                allowEscapeKey: false,
            });
        } else {
            swal.fire({
                title: 'Upss!!',
                icon: 'error',
                text: 'Error desconocido',
                confirmButtonText: 'Ok',
                allowOutsideClick: false,
                allowEscapeKey: false,
            });
            console.log("Error: ", response);
        }

    }

    return (
        <div className="page-container">
            <Sidebar module={'team'} />
            <div className="main-content">
                <Navbar module={'team'} />
                <div className="main-content-inner">
                    <div className='row justify-content-center'>
                    <div className='col-lg-7'>
                    <div className="card mt-5 p-3">
                        <div className="card-body">
                            <div className=" mb-5">
                                <h4>Agregar equipo</h4>
                            </div>
                            <div>
                                <form onSubmit={ onSubmit }>
                                    {/* row */}
                                    <div className="mb-3 row">
                                        <label htmlFor="name" className="col-sm-4 col-form-label form-label"><h6>Nombre</h6></label>
                                        <div className="col-md-8 col-12">
                                            <input type="text" className="form-control form-control2" placeholder="Nombre del equipo" id="name" name='name' value={name} onChange={onChange} required />
                                        </div>
                                    </div>
                                    {/* row */}
                                    <div className="mb-3 row">
                                        <label htmlFor="image" className="col-sm-4 col-form-label form-label"><h6>Imagen</h6></label>
                                        <div className="col-md-8 col-12">
                                            <input type="text" className="form-control form-control2" placeholder="Url imagen del equipo" id="image" name="image" value={image} onChange={onChange} required />
                                        </div>
                                    </div>
                                    {/* row */}
                                    <div className="mb-3 row">
                                        <label htmlFor="country" className="col-sm-4 col-form-label form-label"><h6>País</h6></label>
                                        <div className="col-md-8 col-12">
                                                    <select id="country" name="country" className="form-select form-control2" value={country} onChange={onChange} required >
                                                        <option value={""} defaultValue disabled>Seleccione el país del equipo</option>
                                                        <option value={"Colombia"}> Colombia</option>
                                                        <option value={"Brasil"}>Brasil</option>
                                                    </select>
                                                </div>
                                    </div>
                                    {/* row */}
                                    <div className="mb-3 row">
                                        <label htmlFor="city" className="col-sm-4 col-form-label form-label"><h6>Ciudad</h6></label>
                                        <div className="col-md-8 col-12">
                                            <input type="text" className="form-control form-control2" placeholder="Ciudad del equipo" id="city" name='city' value={city} onChange={onChange} required />
                                        </div>
                                    </div>
                                    {/* row */}
                                    <div className="mb-3 row">
                                        <label htmlFor="founded" className="col-sm-4 col-form-label form-label"><h6>Fecha de fundación</h6></label>
                                        <div className="col-md-8 col-12">
                                            <input type="date" className="form-control form-control2" placeholder="Fecha de fundación del equipo" id="founded" name='founded' value={founded} onChange={onChange}/>
                                        </div>
                                    </div>
                                    {/* row */}
                                    <div className="mb-3 row">
                                        <label htmlFor="coach" className="col-sm-4 col-form-label form-label"><h6>Director Técnico</h6></label>
                                        <div className="col-md-8 col-12">
                                            <input type="text" className="form-control form-control2" placeholder="Director técnico del equipo" id="coach" name='coach' value={coach} onChange={onChange} />
                                        </div>
                                    </div>
                                    <div className="row d-flex justify-content-center pt-4">
                                            <button type="submit" className="btn btn-primary mr-3"><i className="fa fa-save mr-2"></i>Guardar</button>
                                            <Link role="button" to={"/team"} className="btn btn-danger"><i className="fa fa-close mr-2"></i>Cancelar</Link>
                                        </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    </div>
                    </div>
                </div>
            </div>
            <Footer />
        </div>
    )
}

export default CreateTeam;