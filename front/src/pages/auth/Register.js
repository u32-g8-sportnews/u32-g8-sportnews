import React, { useState, useEffect } from 'react';
import APIInvoke from '../../utils/APIInvoke';
import { Link, useNavigate } from 'react-router-dom';
import swal from 'sweetalert2';

const Register = () => {

    const navigate = useNavigate();
    
    const [ user, setUser ] = useState(
        {
            username: '',
            email: '',
            password: '',
            confirm: '',
            role: 'Usuario',
            genre: ''
        }
    );

    const { username, email, password, confirm, genre } = user;

    const onChange = (event) => {
        setUser(
            {
                ...user,
                [ event.target.name ] : event.target.value
            }
        );
    }

    const saveUser = async () => {

        if( password !== confirm ){

            swal.fire({
                title: 'Lo sentimos!',
                icon: 'error',
                text: 'Las contraseñas deben coincidir',
                confirmButtonText: 'Ok',
                allowOutsideClick: false,
                allowEscapeKey: false,
            });

        }else{

            const body = {
                username: user.username,
                email: user.email,
                password: user.password,
                role: user.role,
                genre: user.genre 
            }

            const response = await APIInvoke.invokePOST(`/user/save`, body );

            if ( response.message === 'User is already registered' ){
                
                swal.fire({
                    title: 'Lo sentimos!',
                    icon: 'warning',
                    text: 'El usuario ya esta registrado',
                    confirmButtonText: 'Ok',
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                });
    
            } else if (response.message === 'User saved') {

                swal.fire({
                    title: 'Enhorabuena!',
                    icon: 'success',
                    text: 'Usuario registrado satisfactoriamente',
                    confirmButtonText: 'Ok',
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                });

                setUser(
                    {
                        username: '',
                        email: '',
                        password: '',
                        confirm: '', 
                        genre: ''
                    }
                );

                navigate('/login');
                
            }else {
                swal.fire({
                    title: 'Upss!!',
                    icon: 'error',
                    text: 'Error desconocido',
                    confirmButtonText: 'Ok',
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                });
                console.log( "Error: ", response );
            }
        }
    }

    useEffect( () => {
        document.getElementById("username").focus();
    } , []);

    const onSubmit = (event) => {
        event.preventDefault();
        saveUser();
    } 

    return (
        <div>
            <div className="login-area login-bg">
                <div className="container">
                    <div className="login-box ptb--100">
                        <form onSubmit={onSubmit}>
                            <div className="login-form-head">
                                <h4>Registro</h4>
                                <p>Hola, regístrate y únete a la gran comunidad SportNews</p>
                            </div>
                            <div className="login-form-body">
                                <div className="form-gp">
                                    <input type="text" id="username" name="username" placeholder='Usuario' value={username} onChange={onChange} required/>
                                    <i className="ti-user" />
                                    <div className="text-danger" />
                                </div>
                                <div className="form-gp">
                                    <input type="email" id="email" name="email" placeholder='Correo' value={email} onChange={onChange} required/>
                                    <i className="ti-email"/>
                                    <div className="text-danger"/>
                                </div>
                                <div className="form-gp">
                                    <input type="password" id="password" name="password" placeholder='Contraseña' value={password} onChange={onChange} required/>
                                    <i className="ti-lock" />
                                    <div className="text-danger" />
                                </div>
                                <div className="form-gp">
                                    <input type="password" id="confirm" name="confirm" placeholder='Confirmar contraseña' value={confirm} onChange={onChange} required/>
                                    <i className="ti-lock" />
                                    <div className="text-danger" />
                                </div>
                                <div className="form-gp row border-bottom mx-0">
                                <select id="genre" name="genre" className="form-select form-control2 pl-0 pt-0 rounded-0 border-white" value={genre} onChange={onChange} required >
                                                        <option value={""} defaultValue disabled>Seleccione genero para avatar de usuario</option>
                                                        <option value={"Masculino"}> Masculino</option>
                                                        <option value={"Femenino"}>Femenino</option>
                                                    </select>
                                                    <i className="fa fa-intersex" />
                                </div>
                                <div className="submit-btn-area">
                                    <button id="form_submit" type="submit">Registrarme<i className="ti-arrow-right"/></button>
                                </div>
                                <div className="form-footer text-center mt-5">
                                    <p className="text-muted">Tienes una cuenta? <Link to={"/login"}>Iniciar sesión</Link></p>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Register;