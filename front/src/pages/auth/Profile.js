import React, { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import APIInvoke from "../../utils/APIInvoke";

const Profile = () => {
    const navigate = useNavigate();

    useEffect( () => {
        async function fetchData() {
            const response = await APIInvoke.invokePOST(`/auth/verify`, {} );
            if( response.error !== undefined){
                navigate('/login');
                return;
            }
            document.getElementById("username").innerHTML = response.message.username;
            document.getElementById("role").innerHTML = response.message.role;
            const role = response.message.role;
            localStorage.setItem("username",response.message.username);
            localStorage.setItem("role",response.message.role);
            localStorage.setItem("genre",response.message.genre);
            if (role === "Administrador") {
                navigate('/adminHome');
            } else if (role === "Usuario") {
                navigate('/userHome');
            }else{
                localStorage.setItem("role","Invitado");
                navigate('/');
            }
        }
        fetchData();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return ( 
        <div>
            Bienvenido <span id="username"></span> tienes permisos de <span id="role"></span>
        </div>
    );
}

export default Profile;