import React, { useState, useEffect } from 'react';
import Sidebar from '../../components/Sidebar';
import Footer from '../../components/Footer';
import Navbar from '../../components/Navbar';
import { Link } from 'react-router-dom';
import APIInvoke from '../../utils/APIInvoke';

const UserHome = () => {

    const [teams, setTeams] = useState([]);
    const [news, setNews] = useState([]);
    const [players, setPlayers] = useState([]);
    const [matches, setMatches] = useState([]);
    const [first, setFirst] = useState("");
    const [images, setImages] = useState([]);

    useEffect(() => {
        loadTeams();
        loadMatches();
        loadNews();
        loadPlayers();
    }, [])

    const loadTeams = async () => {
        const response = await APIInvoke.invokeGET(`/team/list`);
        setTeams(response.slice(0, 5));
        setImages(response);
    }

    const loadMatches = async () => {
        const response = await APIInvoke.invokeGET(`/match/list`);
        setMatches(response.slice(0, 3));
        setFirst(response.slice(0, 3)[0]._id);
    }

    const loadNews = async () => {
        const response = await APIInvoke.invokeGET(`/news/list`);
        setNews(response.slice(0, 2));
    }

    const loadPlayers = async () => {
        const response = await APIInvoke.invokeGET(`/player/list`);
        setPlayers(response.slice(0, 8));
    }

    return (
        <div className="page-container">
            <Sidebar module={'home'} />
            <div className="main-content">
                <Navbar module={'home'} />
                <div className="main-content-inner mt-4">
                    <div className="row">
                        <div className="col-lg-4">
                            <div className="card">
                                <div className="card-body">
                                    <h4 className="header-title">Equipos</h4>
                                    <div className="mt-4">
                                        <ul>
                                            {
                                                teams.map(
                                                    item =>
                                                        <li key={item._id} className='mb-4'>
                                                            <div className="d-flex align-items-center">
                                                                <div>
                                                                    <img src={item.image} className="rounded-square avatar-md" alt="teams" />
                                                                </div>
                                                                <div className="ml-4">
                                                                    <h6>{item.name}</h6>
                                                                </div>
                                                            </div>
                                                        </li>
                                                )
                                            }
                                        </ul>
                                    </div>
                                    <div className="trd-history-tabs d-flex justify-content-center align-items-center mt-3">
                                        <ul className="nav">
                                            <li>
                                                <Link className="active" to={"/team"}>Ver más</Link>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-8 mt-sm-30 mt-xs-30">
                            <div className="card">
                                <div className="card-body">
                                    <div className="trd-history-tabs d-flex justify-content-left align-items-center">
                                        <h4 className="header-title mb-2">Próximos encuentros</h4>
                                    </div>
                                    <div id="carouselExampleControls" className="carousel slide" data-ride="carousel">
                                        <div className="carousel-inner">
                                            {
                                                matches.map(
                                                    item =>
                                                        <div key={item._id} className={item._id === first ? "carousel-item active" : "carousel-item"}>
                                                            <div className="d-sm-flex justify-content-center align-items-center">
                                                                <div className="trd-history-tabs">
                                                                    <ul className="nav">
                                                                        <li>
                                                                            <Link className="active" to={"#"}>{item.nameTournament}</Link>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                            <div className="exhcange-rate d-sm-flex justify-content-center align-items-center mt-4">
                                                                <div className='d-flex justify-content-between align-items-center w-75'>
                                                                    <div className='d-flex align-items-center flex-column'>
                                                                        <div>
                                                                            <img src={images.filter(img => img.name===item.local).map(i=>i.image)} className="rounded-square team-md" alt="team" />
                                                                        </div>
                                                                        <h5 className='mt-1'>{item.local}</h5>
                                                                    </div>
                                                                    <h2>VS</h2>
                                                                    <div className='d-flex align-items-center flex-column'>
                                                                        <div>
                                                                            <img src={images.filter(img => img.name===item.visitor).map(i=>i.image)} className="rounded-square team-md" alt="team" />
                                                                        </div>
                                                                        <h5 className='mt-1'>{item.visitor}</h5>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="exhcange-rate d-sm-flex justify-content-center align-items-center mt-2">
                                                                <h6>{item.date.split("T")[0]} | {item.date.split("T")[1]} | {item.place}</h6>
                                                            </div>
                                                        </div>
                                                )}
                                        </div>
                                        <a className="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                                            <span className="carousel-control-prev-icon" aria-hidden="true" />
                                            <span className="sr-only">Previous</span>
                                        </a>
                                        <a className="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                                            <span className="carousel-control-next-icon" aria-hidden="true" />
                                            <span className="sr-only">Next</span>
                                        </a>
                                    </div>
                                    <div className="trd-history-tabs d-flex justify-content-center align-items-center mt-3">
                                        <ul className="nav">
                                            <li>
                                                <Link className="active" to={"/match"}>Ver más</Link>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row mt-5">
                        <div className="col-xl-7">
                            <div className="card">
                                <div className="card-body">
                                    <h4 className="header-title">Últimas Noticias</h4>
                                    <div className="letest-news mt-5">
                                        {
                                            news.map(
                                                item =>
                                                    <div key={item._id} className="single-post">
                                                        <div className="lts-thumb">
                                                            <img src={item.image ? item.image : "/assets/images/news/news.jpg"} alt="post thumb" />
                                                        </div>
                                                        <div className="lts-content">
                                                            <h2><Link to={"/news"}>{item.title}</Link></h2>
                                                            <p>{item.content}</p>
                                                            <span className='pull-right'>{item.author}</span>
                                                        </div>
                                                    </div>
                                            )
                                        }
                                    </div>
                                    <div className="trd-history-tabs d-flex justify-content-center align-items-center mt-3">
                                        <ul className="nav">
                                            <li>
                                                <Link className="active" to={"/news"}>Ver más</Link>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-xl-5 mt-md-30 mt-xs-30 mt-sm-30">
                            <div className="card">
                                <div className="card-body">
                                    <h4 className="header-title mb-4">Jugadores</h4>
                                    {
                                        players.map(
                                            item =>
                                                <div key={item._id} className="d-flex justify-content-between align-items-center mb-4">
                                                    <div className="d-flex align-items-center">
                                                        <div>
                                                            <img src={item.image} className="rounded-circle avatar-md" alt="players" />
                                                        </div>
                                                        <div className="ml-4">
                                                            <h6>{item.name}</h6>
                                                            <p className="text-muted mb-0 fs-5 text-muted">{item.position}</p>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <p className="text-muted mb-0 text-muted">{item.nameTeam}</p>
                                                    </div>
                                                </div>
                                        )
                                    }
                                    <div className="trd-history-tabs d-flex justify-content-center align-items-center mt-3">
                                        <ul className="nav">
                                            <li>
                                                <Link className="active" to={"/player"}>Ver más</Link>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <Footer />
        </div>
    )
}

export default UserHome;