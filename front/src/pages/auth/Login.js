import React, { useState, useEffect } from 'react';
import APIInvoke from '../../utils/APIInvoke';
import { Link, useNavigate } from 'react-router-dom';
import swal from 'sweetalert2';

const Login = () => {

    const navigate = useNavigate();

    const [ user, setUser ] = useState(
        {
            username: '',
            password: ''
        }
    );

    const { username, password } = user;

    const onChange = (event) => {
        setUser(
            {
                ...user,
                [ event.target.name ] : event.target.value
            }
        );
    }

    const login = async () => {
        
        const body = {
            username: user.username,
            password: user.password 
        }

        const response = await APIInvoke.invokePOST(`/auth/login`, body);

        if ( response.message === "Invalid credentials" ){
            
            swal.fire({
                title: 'Usuario inválido',
                icon: 'warning',
                text: 'No tienes una cuenta registrada',
                confirmButtonText: 'Ok',
                allowOutsideClick: false,
                allowEscapeKey: false
            });

        }else if  ( response.message === "Invalid password" ){

            swal.fire({
                title: 'Contraseña incorrecta',
                icon: 'warning',
                text: 'La contraseña es incorrecta',
                confirmButtonText: 'Ok',
                allowOutsideClick: false,
                allowEscapeKey: false
            });

        }else if ( response.message === "Authentication successful" ){   
            localStorage.setItem('token', response.token );
            navigate('/profile');
        }else {
            swal.fire({
                title: 'Upss!!',
                icon: 'error',
                text: 'Error desconocido',
                confirmButtonText: 'Ok',
                allowOutsideClick: false,
                allowEscapeKey: false,
            });
            console.log( "Error: ", response );
        }
    };

    const onSubmit = ( event ) => {
        event.preventDefault();
        login();
    };
    
    useEffect( () => {
        document.getElementById("username").focus();
    } , []);

    return (
        <div>
            <div className="login-area login-bg">
                <div className="container">
                    <div className="login-box ptb--100">
                        <form onSubmit={onSubmit}>
                            <div className="login-form-head">
                                <h4>Inicio de sesión</h4>
                                <p>Hola, conéctate y descubre las novedades del mundo del deporte</p>
                            </div>
                            <div className="login-form-body">
                                <div className="form-gp">
                                    <input type="text" id="username" name= "username" value={username} onChange={onChange} placeholder='Usuario'/>
                                    <i className="ti-user" />
                                    <div className="text-danger" />
                                </div>
                                <div className="form-gp">
                                    <input type="password" id="password" name= "password" value={password} onChange={onChange} placeholder='Contraseña'/>
                                    <i className="ti-lock" />
                                    <div className="text-danger" />
                                </div>
                                <div className="row mb-4 rmber-area">
                                    <div className="col-5">
                                        <div className="custom-control custom-checkbox mr-sm-2">
                                            <input type="checkbox" className="custom-control-input" id="customControlAutosizing" />
                                            <label className="custom-control-label" htmlFor="customControlAutosizing">Recordarme</label>
                                        </div>
                                    </div>
                                    <div className="col-7 text-right">
                                        <Link to={"#"}>Olvidaste tu contraseña?</Link>
                                    </div>
                                </div>
                                <div className="submit-btn-area">
                                    <button id="form_submit" type="submit">Iniciar<i className="ti-arrow-right"/></button>
                                </div>
                                <div className="form-footer text-center mt-5">
                                    <p className="text-muted">No tienes una cuenta? <Link to={"/register"}>Registrarme</Link></p>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Login;