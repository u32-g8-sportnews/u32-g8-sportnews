import React from 'react'
import { Link } from 'react-router-dom'

const NotFound = () => {

    const role = localStorage.getItem("role");

    return (
        <div className="ptb--100 text-center login-bg">
            <div className="container">
                <div className="error-content">
                    <h2>404</h2>
                    <p><h4>¡Ooops! Algo salió mal</h4></p>
                    <h6 className='mt-3'>Pagina no encontrada</h6>
                    <Link to={role==="Invitado"?"/":(role==="Administrador"?"/adminHome":"/userHome")} className='w-25'><h5>Inicio</h5></Link>
                </div>
            </div>
        </div>
    )
}

export default NotFound