import React, { useState, useEffect } from 'react';
import Sidebar from '../../components/Sidebar';
import Footer from '../../components/Footer';
import Navbar from '../../components/Navbar';
import { Link, useNavigate, useParams } from 'react-router-dom';
import APIInvoke from '../../utils/APIInvoke';
import swal from 'sweetalert2';

const UpdateUser = () => {

    let navigate = useNavigate();

    const { id } = useParams();

    useEffect(() => {

        async function loadUser() {
            const response = await APIInvoke.invokeGET(`/user/${id}`);
            setUser(response);
            return;
        }

        loadUser();

        document.getElementById("username").focus();
    }, []);

    const [user, setUser] = useState(
        {
            username: '',
            email: '',
            password: '',
            role: '',
            genre: ''
        }
    )

    const { username, email, password, role, genre } = user;

    const onChange = (e) => {
        setUser({
            ...user,
            [e.target.name]: e.target.value
        });
    }

    const onSubmit = (e) => {
        e.preventDefault();
        updateUser();
    }

    const updateUser = async () => {
        const data = {
            username: user.username,
            email: user.email,
            password: user.password,
            role: user.role,
            genre: user.genre
        }

        const response = await APIInvoke.invokePUT(`/user/${id}`, data);

        if (response.message === "User updated") {
            swal.fire({
                title: 'Enhorabuena!',
                icon: 'success',
                text: 'Usuario actualizado satisfactoriamente',
                confirmButtonText: 'Ok',
                allowOutsideClick: false,
                allowEscapeKey: false,
            }).then((result) => {
                if (result.isConfirmed) {
                    navigate('/user');
                }
            })
        } else if(response.message === "User is already registered"){
            swal.fire({
                title: 'Lo sentimos!',
                icon: 'warning',
                text: 'El usuario ya esta registrado',
                confirmButtonText: 'Ok',
                allowOutsideClick: false,
                allowEscapeKey: false,
            });
        }else {
            swal.fire({
                title: 'Upss!!',
                icon: 'error',
                text: 'Error desconocido',
                confirmButtonText: 'Ok',
                allowOutsideClick: false,
                allowEscapeKey: false,
            });
            console.log("Error: ", response);
        }
    }

    return (
        <div className="page-container">
            <Sidebar module={'user'} />
            <div className="main-content">
                <Navbar module={'user'} />
                <div className="main-content-inner">
                    <div className='row justify-content-center'>
                        <div className='col-lg-7'>
                            <div className="card mt-5 p-3">
                                <div className="card-body">
                                    <div className=" mb-5">
                                        <h4>Actualizar usuario</h4>
                                    </div>
                                    <div>
                                        <form onSubmit={onSubmit}>
                                            {/* row */}
                                            <div className="mb-3 row">
                                                <label htmlFor="username" className="col-sm-4 col-form-label form-label"><h6>Usuario</h6></label>
                                                <div className="col-sm-8 mb-3 mb-lg-0">
                                                    <input type="text" className="form-control form-control2" placeholder="Nombre de usuario" id="username" name="username" value={username} onChange={onChange} required />
                                                </div>
                                            </div>
                                            {/* row */}
                                            <div className="mb-3 row">
                                                <label htmlFor="email" className="col-sm-4 col-form-label form-label"><h6>Correo</h6></label>
                                                <div className="col-md-8 col-12">
                                                    <input type="email" className="form-control form-control2" placeholder="Correo electronico" id="email" name="email" value={email} onChange={onChange} required />
                                                </div>
                                            </div>
                                            {/* row */}
                                            <div className="mb-3 row">
                                                <label htmlFor="password" className="col-sm-4 col-form-label form-label"><h6>Contraseña</h6></label>
                                                <div className="col-md-8 col-12">
                                                    <input type="text" className="form-control form-control2" placeholder="Contraseña del usuario" id="password" name="password" value={password} onChange={onChange} required />
                                                </div>
                                            </div>
                                            {/* row */}
                                            <div className="mb-3 row">
                                                <label htmlFor="role" className="col-sm-4 col-form-label form-label"><h6>Rol</h6></label>
                                                <div className="col-md-8 col-12">
                                                    <select id="role" name="role" className="form-select form-control2" value={role} onChange={onChange} required >
                                                        <option value={""} defaultValue disabled>Seleccione el nivel de acceso del usuario</option>
                                                        <option value={"Administrador"}> Administrador</option>
                                                        <option value={"Usuario"}>Usuario</option>
                                                    </select>
                                                </div>
                                            </div>
                                            {/* row */}
                                            <div className="mb-3 row">
                                                <label htmlFor="genre" className="col-sm-4 col-form-label form-label"><h6>Avatar</h6></label>
                                                <div className="col-md-8 col-12">
                                                    <select id="genre" name="genre" className="form-select form-control2" value={genre} onChange={onChange} required >
                                                        <option value={""} defaultValue disabled>Seleccione avatar de usuario</option>
                                                        <option value={"Masculino"}>Masculino</option>
                                                        <option value={"Femenino"}>Femenino</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div className="row d-flex justify-content-center pt-4">
                                                <button type="submit" className="btn btn-primary mr-3"><i className="fa fa-save mr-2"></i>Guardar</button>
                                                <Link role="button" to={"/user"} className="btn btn-danger"><i className="fa fa-close mr-2"></i>Cancelar</Link>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <Footer />
        </div>
    )
}

export default UpdateUser;