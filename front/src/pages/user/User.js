import React, { useState, useEffect } from 'react';
import Sidebar from '../../components/Sidebar';
import Footer from '../../components/Footer';
import Navbar from '../../components/Navbar';
import { Link } from 'react-router-dom';
import APIInvoke from '../../utils/APIInvoke';
import swal from 'sweetalert2';

const User = () => {

    const [users, setUsers] = useState([]);
    const [search, setSearch] = useState("");

    useEffect(() => {
        loadUsers();
    }, [])

    const searchEvent = async () => {
        const response = await APIInvoke.invokeGET(`/user/list/${search}`);
        setUsers(response);
    };

    const onSubmit = (e) => {
        e.preventDefault();
        searchEvent();
    }

    const onChange = (e) => {
        setSearch(e.target.value);
    }

    const loadUsers = async () => {
        const response = await APIInvoke.invokeGET(`/user/list`);
        setUsers(response);
    }

    const deleteUser = async (e, id) => {

        e.preventDefault();

        swal.fire({
            icon: 'question',
            title: '¿Estas seguro?',
            text: '¿Deseas eliminar el usuario?',
            showCancelButton: true,
            confirmButtonText: 'Eliminar',
            cancelButtonText: 'Cancelar'
        }).then(async (result) => {
            if (result.isConfirmed) {
                const response = await APIInvoke.invokeDELETE(`/user/${id}`);
                if (response.message === "User deleted") {
                    swal.fire(
                        'Eliminación exitosa',
                        'El usuario fue eliminado',
                        'success'
                    )
                    loadUsers();
                }
            }
        })
    }

    return (
        <div className="page-container">
            <Sidebar module={'user'} />
            <div className="main-content">
                <Navbar module={'user'} />
                <div className="main-content-inner mt-4">
                    <div className='row mt-5'>
                        <div className="col-xl-12 col-lg-12 col-md-12 col-12">
                            <div className="card">
                                <div className='card-body'>
                                    <h4 className="header-title py-3">Usuarios </h4>
                                    <div className='row align-middle pb-4 justify-content-between'>
                                        <div className="search-box pull-left col-lg-6">
                                            <form onSubmit={onSubmit}>
                                                <input className='w-100 form-control input-rounded' type="text" name="search" placeholder="Buscar usuarios por nombre, email o rol" value={search} onChange={onChange} />
                                                <i className="ti-search" />
                                            </form>
                                        </div>
                                        <div className="mr-3">
                                            <Link to={"/user/create"} className="btn btn-primary"><i className="fa fa-plus-circle mr-2" />Agregar usuario</Link>
                                        </div>
                                    </div>

                                    <div className="table-responsive">
                                        <table className="table text-left">
                                            <thead className="bg-warning">
                                                <tr className='text-white'>
                                                    <th>Nombre</th>
                                                    <th>Rol</th>
                                                    <th>Fecha creación</th>
                                                    <th>Acciones</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {
                                                    users.map(
                                                        item =>
                                                            <tr key={item._id}>
                                                                <td className="col-5 align-middle">
                                                                    <div className="d-flex align-items-center">
                                                                        <div>
                                                                            <img src={item.genre === "Masculino"?(item.role==="Administrador"?"/assets/images/avatar/avatar-1.jpg":"/assets/images/avatar/avatar-3.jpg"):(item.role==="Administrador"?"/assets/images/avatar/avatar-4.jpg":"/assets/images/avatar/avatar-2.jpg")} alt="avatar" className="avatar-md avatar rounded-circle" />
                                                                        </div>
                                                                        <div className="ml-3">
                                                                            <h6>{item.username}</h6>
                                                                            <p>{item.email}</p>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td className="col-3 align-middle">{item.role}</td>
                                                                <td className="col-4 align-middle">{item.createdAt}</td>
                                                                <td className="col-2 align-middle">
                                                                    <div className="d-flex justify-content-left">
                                                                        <Link to={`/user/update/${item._id}`} className="btn btn-primary btn-xs mr-3"><i className="fa fa-edit" /></Link>
                                                                        <Link to={"#"} role='button' onClick={(e) => { deleteUser(e, item._id) }} className="btn btn-danger btn-xs"><i className="fa fa-trash-o" /></Link>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                    )}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <Footer />
        </div>
    )
}

export default User;