import React, { Fragment } from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Home from './pages/Home';
import Register from './pages/auth/Register';
import Login from './pages/auth/Login';
import Profile from './pages/auth/Profile';
import AdminHome from './pages/auth/AdminHome';
import UserHome from './pages/auth/UserHome';
import NotFound from './pages/NotFound';
import User from './pages/user/User';
import CreateUser from './pages/user/CreateUser';
import UpdateUser from './pages/user/UpdateUser';
import Match from './pages/match/Match';
import CreateMatch from './pages/match/CreateMatch';
import UpdateMatch from './pages/match/UpdateMatch';
import News from './pages/news/News';
import CreateNews from './pages/news/CreateNews';
import UpdateNews from './pages/news/UpdateNews';
import Player from './pages/player/Player';
import CreatePlayer from './pages/player/CreatePlayer';
import UpdatePlayer from './pages/player/UpdatePlayer';
import Team from './pages/team/Team';
import CreateTeam from './pages/team/CreateTeam';
import UpdateTeam from './pages/team/UpdateTeam';

function App() {
  return (
    <Fragment>
      <Router>
        <Routes>
          <Route path='/' exact element={ <Home/> }/>
          <Route path="*" element={<NotFound/>} />
          <Route path='/register' exact element={ <Register/> }/>
          <Route path='/login' exact element={ <Login/> }/>
          <Route path='/profile' exact element={ <Profile/> }/>
          <Route path='/adminHome' exact element={ <AdminHome/> }/>
          <Route path='/userHome' exact element={ <UserHome/> }/>   
          <Route path='/user' exact element={ <User/> }/>
          <Route path='/user/create' exact element={ <CreateUser/> }/>
          <Route path='/user/update/:id' exact element={ <UpdateUser/> }/>
          <Route path='/match' exact element={ <Match/> }/>
          <Route path='/match/create' exact element={ <CreateMatch/> }/>
          <Route path='/match/update/:id' exact element={ <UpdateMatch/> }/>
          <Route path='/news' exact element={ <News/> }/>
          <Route path='/news/create' exact element={ <CreateNews/> }/>
          <Route path='/news/update/:id' exact element={ <UpdateNews/> }/>
          <Route path='/player' exact element={ <Player/> }/>
          <Route path='/player/create' exact element={ <CreatePlayer/> }/>
          <Route path='/player/update/:id' exact element={ <UpdatePlayer/> }/>
          <Route path='/team' exact element={ <Team/> }/>
          <Route path='/team/create' exact element={ <CreateTeam/> }/>
          <Route path='/team/update/:id' exact element={ <UpdateTeam/> }/>
        </Routes>
      </Router>
    </Fragment>
  );
}

export default App;