const { Router } = require('express');
const userController = require('../controllers/userController');
const newsController = require('../controllers/newsController');
const tournamentController = require('../controllers/tournamentController');
const matchController = require('../controllers/matchController');
const teamController = require('../controllers/teamController');
const playerController = require('../controllers/playerController');
const authController = require('../controllers/authController');

let router = Router();

// Auth
router.post('/auth/login', authController.login);
router.post('/auth/verify', authController.verifyToken, authController.profile);

// Users
router.get( '/user/list/:search?', userController.listUsers );
router.get( '/user/:id', userController.findUser );
router.post( '/user/save', userController.saveUser );
router.put( '/user/:id', userController.updateUser );
router.delete( '/user/:id', userController.deleteUser );

// News
router.get( '/news/list/:search?', newsController.listNews );
router.get( '/news/:id', newsController.findNews );
router.post( '/news/save', authController.verifyToken, newsController.saveNews );
router.put( '/news/:id', authController.verifyToken, newsController.updateNews );
router.delete( '/news/:id', authController.verifyToken, newsController.deleteNews );

// Tournaments
router.get( '/tournament/list/:search?', tournamentController.listTournaments );
router.get( '/tournament/:id', tournamentController.findTournament );
router.post( '/tournament/save', authController.verifyToken, tournamentController.saveTournament );
router.put( '/tournament/:id', authController.verifyToken, tournamentController.updateTournament );

// Matchs
router.get( '/match/list/:search?', matchController.listMatchs );
router.get( '/match/:id', matchController.findMatch );
router.post( '/match/save', authController.verifyToken, matchController.saveMatch );
router.put( '/match/:id', authController.verifyToken, matchController.updateMatch );
router.delete( '/match/:id', authController.verifyToken, matchController.deleteMatch );

// Teams
router.get( '/team/list/:search?', teamController.listTeams );
router.get( '/team/:id', teamController.findTeam );
router.post( '/team/save', authController.verifyToken, teamController.saveTeam );
router.put( '/team/:id', authController.verifyToken, teamController.updateTeam );
router.delete( '/team/:id', authController.verifyToken, teamController.deleteTeam );

// Players
router.get( '/player/list/:search?', playerController.listPlayers );
router.get( '/player/:id', playerController.findPlayer );
router.post( '/player/save', authController.verifyToken, playerController.savePlayer );
router.put( '/player/:id', authController.verifyToken, playerController.updatePlayer );
router.delete( '/player/:id', authController.verifyToken, playerController.deletePlayer );

module.exports = router;