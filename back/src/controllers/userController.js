let User = require('../models/user');
const bcrypt = require('bcrypt');

async function saveUser(req, res) {

    let newUser = new User(req.body);
    
    const userFound = await User.findOne({
        username: newUser.username
    });
    
    if (userFound != null) {
        res.status(403).send({ message: 'User is already registered' });
        return;
    } else {
        // Generate salt to hash password
        const salt = await bcrypt.genSalt(10);
        newUser.password = await bcrypt.hash(newUser.password, salt);

        newUser.save((err, result) => {
            if (err) {
                res.status(500).send({ message: err });
            } else {
                res.status(200).send({ message: "User saved", result: result });
            }
        });
    }
}

function listUsers(req, res) {

    let search = req.params.search;

    let queryParam = {};

    if (search) {

        queryParam = {
            $or: [
                { username: { $regex: search, $options: "i" } },
                { email: { $regex: search, $options: "i" } },
                { role: { $regex: search, $options: "i" } },
                { genre: { $regex: search, $options: "i" } }
            ]
        };
    }

    query = User.find(queryParam).sort('createdAt');


    query.exec((err, result) => {
        if (err) {
            res.status(500).send({ message: err });
        } else {
            res.status(200).send( result );
        }
    });

}

function findUser(req, res) {

    let id = req.params.id;
    let query = User.findById(id);

    query.exec((err, result) => {
        if (err) {
            res.status(500).send({ message: err });
        } else {
            res.status(200).send(result);
        }
    });
}

function updateUser(req, res) {

    let id = req.params.id;
    let data = req.body;

    User.findByIdAndUpdate(id, data, { new: true }, (err, result) => {
        if (err) {
            res.status(500).send({ message: err });
        } else {
            res.status(200).send({ message: "User updated", result: result });
        }
    });
}

function deleteUser(req, res) {

    let id = req.params.id;

    User.findByIdAndDelete(id, (err, result) => {
        if (err) {
            res.status(500).send({ message: err });
        } else {
            res.status(200).send({ message: "User deleted", result: result });
        }
    });
}

module.exports = { saveUser, listUsers, findUser, updateUser, deleteUser }