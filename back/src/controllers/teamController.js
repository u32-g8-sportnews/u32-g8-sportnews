let Team = require('../models/team');

async function saveTeam(req, res) {

    let newTeam = new Team(req.body);
    
    const teamFound = await Team.findOne({
        name: newTeam.name
    });
    
    if (teamFound != null) {
        res.status(403).send({ message: 'Team is already registered' });
        return;
    } else {
        newTeam.save((err, result) => {
            if (err) {
                res.status(500).send({ message: err });
            } else {
                res.status(200).send({ message: "Team saved", result: result });
            }
        });
    }
}

function listTeams(req, res) {

    let search = req.params.search;

    let queryParam = {};

    if (search) {

        queryParam = {
            $or: [
                { name: { $regex: search, $options: "i" } },
                { country: { $regex: search, $options: "i" } },
                { city: { $regex: search, $options: "i" } }
            ]
        };
    }

    query = Team.find(queryParam).sort('name');

    query.exec((err, result) => {
        if (err) {
            res.status(500).send({ message: err });
        } else {
            res.status(200).send(result);
        }
    });

}

function findTeam(req, res) {

    let id = req.params.id;
    let query = Team.findById(id);

    query.exec((err, result) => {
        if (err) {
            res.status(500).send({ message: err });
        } else {
            res.status(200).send(result);
        }
    });
}

function updateTeam(req, res) {

    let id = req.params.id;
    let data = req.body;

    Team.findByIdAndUpdate(id, data, { new: true }, (err, result) => {
        if (err) {
            res.status(500).send({ message: err });
        } else {
            res.status(200).send({ message: "Team updated", result: result });
        }
    });
}

function deleteTeam(req, res) {

    let id = req.params.id;

    Team.findByIdAndDelete(id, (err, result) => {
        if (err) {
            res.status(500).send({ message: err });
        } else {
            res.status(200).send({ message: "Team deleted", result: result });
        }
    });
}

module.exports = { saveTeam, listTeams, findTeam, updateTeam, deleteTeam }