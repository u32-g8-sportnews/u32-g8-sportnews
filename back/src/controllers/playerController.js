let Player = require('../models/player');

async function savePlayer(req, res) {

    let newPlayer = new Player(req.body);
    
    const playerFound = await Player.findOne({
        name: newPlayer.name
    });
    
    if (playerFound != null) {
        res.status(403).send({ message: 'Player is already registered' });
        return;
    } else {
        newPlayer.save((err, result) => {
            if (err) {
                res.status(500).send({ message: err });
            } else {
                res.status(200).send({ message: "Player saved", result: result });
            }
        });
    }
}

function listPlayers(req, res) {

    let search = req.params.search;

    let queryParam = {};

    if (search) {

        queryParam = {
            $or: [
                { name: { $regex: search, $options: "i" } },
                { nacionality: { $regex: search, $options: "i" } },
                { nameTeam: { $regex: search, $options: "i" } },
                { position: { $regex: search, $options: "i" } }
            ]
        };
    }

    query = Player.find(queryParam).sort('name');

    query.exec((err, result) => {
        if (err) {
            res.status(500).send({ message: err });
        } else {
            res.status(200).send(result);
        }
    });

}

function findPlayer(req, res) {

    let id = req.params.id;
    let query = Player.findById(id);

    query.exec((err, result) => {
        if (err) {
            res.status(500).send({ message: err });
        } else {
            res.status(200).send(result);
        }
    });
}

function updatePlayer(req, res) {

    let id = req.params.id;
    let data = req.body;

    Player.findByIdAndUpdate(id, data, { new: true }, (err, result) => {
        if (err) {
            res.status(500).send({ message: err });
        } else {
            res.status(200).send({ message: "Player updated", result: result });
        }
    });
}

function deletePlayer(req, res) {

    let id = req.params.id;

    Player.findByIdAndDelete(id, (err, result) => {
        if (err) {
            res.status(500).send({ message: err });
        } else {
            res.status(200).send({ message: "Player deleted", result: result });
        }
    });
}

module.exports = { savePlayer, listPlayers, findPlayer, updatePlayer, deletePlayer }