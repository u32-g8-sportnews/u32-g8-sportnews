let Tournament = require('../models/tournament');

async function saveTournament(req, res) {

    let newTournament = new Tournament(req.body);
    
    const tournamentFound = await Tournament.findOne({
        name: newTournament.name,
        season: newTournament.season
    });
    
    if (tournamentFound != null) {
        res.status(403).send({ message: 'Tournament is already registered' });
        return;
    } else {
        newTournament.save((err, result) => {
            if (err) {
                res.status(500).send({ message: err });
            } else {
                res.status(200).send({ message: "Tournament saved", result: result });
            }
        });
    }
}

function listTournaments(req, res) {

    let search = req.params.search;

    let queryParam = {};

    if (search) {

        queryParam = {
            $or: [
                { name: { $regex: search, $options: "i" } },
                { season: { $regex: search, $options: "i" } }
            ]
        };
    }

    query = Tournament.find(queryParam).sort('createdAt');

    query.exec((err, result) => {
        if (err) {
            res.status(500).send({ message: err });
        } else {
            res.status(200).send(result);
        }
    });

}

function findTournament(req, res) {

    let id = req.params.id;
    let query = Tournament.findById(id);

    query.exec((err, result) => {
        if (err) {
            res.status(500).send({ message: err });
        } else {
            res.status(200).send({ message: result });
        }
    });
}

function updateTournament(req, res) {

    let id = req.params.id;
    let data = req.body;

    Tournament.findByIdAndUpdate(id, data, { new: true }, (err, result) => {
        if (err) {
            res.status(500).send({ message: err });
        } else {
            res.status(200).send({ message: "Tournament updated", result: result });
        }
    });
}

module.exports = { saveTournament, listTournaments, findTournament, updateTournament }