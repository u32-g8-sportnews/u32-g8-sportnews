let News = require('../models/news');

function saveNews(req, res) {

    let myNews = new News(req.body);
    myNews.author = req.data.username;

    myNews.save((err, result) => {
        if (err) {
            res.status(500).send({ message: err });
        } else {
            res.status(200).send({ message: "News saved", result: result });
        }
    });
}

function listNews(req, res) {

    let search = req.params.search;
    let queryParam = {};

    if (search) {
        queryParam = {
            $or: [
                { title: { $regex: search, $options: "i" } },
                { keywords: { $regex: search, $options: "i" } },
                { author: { $regex: search, $options: "i" } }
            ]
        };
    }

    query = News.find(queryParam).sort({ 'updatedAt': -1 });

    query.exec((err, result) => {
        if (err) {
            res.status(500).send({ message: err });
        } else {
            res.status(200).send(result);
        }
    });

}

function findNews(req, res) {

    let id = req.params.id;
    let query = News.findById(id);

    query.exec((err, result) => {
        if (err) {
            res.status(500).send({ message: err });
        } else {
            res.status(200).send(result);
        }
    });
}

function updateNews(req, res) {

    let id = req.params.id;
    let data = req.body;

    News.findByIdAndUpdate(id, data, { new: true }, (err, result) => {
        if (err) {
            res.status(500).send({ message: err });
        } else {
            res.status(200).send({ message: "News updated", result: result });
        }
    });
}

function deleteNews(req, res) {

    let id = req.params.id;

    News.findByIdAndDelete(id, (err, result) => {
        if (err) {
            res.status(500).send({ message: err });
        } else {
            res.status(200).send({ message: "News deleted", result: result });
        }
    });
}

module.exports = { saveNews, listNews, findNews, updateNews, deleteNews }