let Match = require('../models/match');

async function saveMatch(req, res) {

    let newMatch = new Match(req.body);
    
    const matchFound = await Match.findOne({
        nameTournament: newMatch.nameTournament,
        seasonTournament: newMatch.seasonTournament,
        date: newMatch.date,
        place: newMatch.place,
        local: newMatch.local,
        visitor: newMatch.visitor
    });
    
    if (matchFound != null) {
        res.status(403).send({ message: 'Match is already registered' });
        return;
    } else {
        newMatch.save((err, result) => {
            if (err) {
                res.status(500).send({ message: err });
            } else {
                res.status(200).send({ message: "Match saved", result: result });
            }
        });
    }
}

function listMatchs(req, res) {

    let search = req.params.search;

    let queryParam = {};

    if (search) {

        queryParam = {
            $or: [
                { date: { $regex: search, $options: "i" } },
                { place: { $regex: search, $options: "i" } },
                { nameTournament: { $regex: search, $options: "i" } },
                { local: { $regex: search, $options: "i" } },
                { visitor: { $regex: search, $options: "i" } }
            ]
        };
    }

    query = Match.find(queryParam).sort('date');

    query.exec((err, result) => {
        if (err) {
            res.status(500).send({ message: err });
        } else {
            res.status(200).send(result);
        }
    });

}

function findMatch(req, res) {

    let id = req.params.id;
    let query = Match.findById(id);

    query.exec((err, result) => {
        if (err) {
            res.status(500).send({ message: err });
        } else {
            res.status(200).send(result);
        }
    });
}

function updateMatch(req, res) {

    let id = req.params.id;
    let data = req.body;

    Match.findByIdAndUpdate(id, data, { new: true }, (err, result) => {
        if (err) {
            res.status(500).send({ message: err });
        } else {
            res.status(200).send({ message: "Match updated", result: result });
        }
    });
}

function deleteMatch(req, res) {

    let id = req.params.id;

    Match.findByIdAndDelete(id, (err, result) => {
        if (err) {
            res.status(500).send({ message: err });
        } else {
            res.status(200).send({ message: "Match deleted", result: result });
        }
    });
}

module.exports = { saveMatch, listMatchs, findMatch, updateMatch, deleteMatch }