const mongoose = require('mongoose');

let Schema = mongoose.Schema;

const playerSchema = new Schema({
    code: { type: Number, unique: true },
    name: { type: String, required: true },
    age: { type: Number, required: true },
    image: { type: String },
    position: { type: String, required: true },
    nacionality: { type: String, required: true },
    nameTeam: { type: String }
    });
    
const Player = mongoose.model('players', playerSchema);

module.exports = Player;