const mongoose = require('mongoose');

let Schema = mongoose.Schema;

const matchSchema = new Schema({
    nameTournament: { type: String, required: true },
    seasonTournament: { type: Number, required: true },
    date: { type: String, required: true },
    place: { type: String, required: true },
    local: { type: String, required: true },
    localScore: { type: String },
    visitor: { type: String, required: true },
    visitorScore: { type: String }
    }
);

const Match = mongoose.model('matchs', matchSchema);

module.exports = Match;