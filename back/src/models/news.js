const mongoose = require('mongoose');

let Schema = mongoose.Schema;

const newsSchema = new Schema({
    title: { type: String, required: true },
    content: { type: String, required: true },
    image: { type: String },
    keywords: { type: String },
    author: { type: String, required: true }
    }, 
    { 
        timestamps: true 
    });

const News = mongoose.model('news', newsSchema);

module.exports = News;