const mongoose = require('mongoose');

let Schema = mongoose.Schema;

const teamSchema = new Schema({
    name: { type: String, required: true },
    image: { type: String },
    country: { type: String, required: true},
    city: { type: String, required: true},
    founded: { type: String},
    coach: { type: String},
    players: [{
        namePlayer: { type: String },
        imagePlayer: { type: String },
        position: { type: String }
    }]
    });
    
const Team = mongoose.model('teams', teamSchema);

module.exports = Team;