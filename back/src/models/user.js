const mongoose = require('mongoose');

let Schema = mongoose.Schema;

const userSchema = new Schema({
    username: { type: String, required: true },
    email: { type: String, required: true, unique: true},
    password: { type: String, required: true },
    role: { type: String, required: true },
    genre: {type: String, required: true},
    }, 
    { 
        timestamps: true 
    });

const User = mongoose.model('users', userSchema);

module.exports = User;