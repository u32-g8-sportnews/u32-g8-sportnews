const mongoose = require('mongoose');

let Schema = mongoose.Schema;

const tournamentSchema = new Schema({
    name: { type: String, required: true },
    season: { type: Number, required: true },
    rating: [
        {
            team: { type: String },
            matchesPlayed: { type: Number },
            matchesWon: { type: Number },
            matchesLost: { type: Number },
            matchesTied: { type: Number },
            points: { type: Number }
        }
    ]}
);

const Tournament = mongoose.model('tournaments', tournamentSchema);

module.exports = Tournament;